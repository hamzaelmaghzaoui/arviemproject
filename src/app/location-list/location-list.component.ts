import { Component, OnInit,ViewChild } from '@angular/core';
 import { MatTableDataSource,MatSort,MatPaginator, MatDialogConfig } from '@angular/material';
import { HttpCallService } from '../services/http-call.service';
import { FormsModule } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'arviem-location-list',
  templateUrl: './location-list.component.html',
  styleUrls: ['./location-list.component.css']
})
export class LocationListComponent implements OnInit {

  uri : string = '/tenant1/locations';
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['name', 'normalizedName', 'coordinates', 'country', 'resourceId','version','function','actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;
  pagenumber: Number = 0;
  pagesize: Number= 50;

  constructor(private httpCallSvc: HttpCallService,public dialog: MatDialog){}
  ngOnInit(): void {
    
  }

  /**
   * fitch Data
   */
  public getDataFromServer() {

    this.httpCallSvc.callGetHttpRequest(this.uri,this.pagenumber,this.pagesize).subscribe(
      (data: any)=>{
        this.listData = new MatTableDataSource(data);
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele != 'version' &&
                   ele != 'coordinates' &&
                   ele != 'actions' &&
                   data[ele] != undefined &&
                   data[ele].trim().toLowerCase().indexOf(filter) != -1;
          });
        };
      });

  }

  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  openDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.position = {
      'top': '0',
      left: '0'
  };

    const dialogRef = this.dialog.open(ModalComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      
    });
  }
  


}
