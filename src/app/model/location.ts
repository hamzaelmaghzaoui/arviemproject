
import {Coordinates} from "./coordinates";
import {Address} from "./address";

export class Location { 
    name: string ; 
    normalizedName: string ;
    coordinates: Coordinates;
    address: Address;
    resourceId: string;
    version: Number;
    function: string;
}
