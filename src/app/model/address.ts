export class Address{
       
       city:string;
       country:string;
       continent:string;
       streetName:string;
       streetNumber:Number;
       postalCode:Number;
       administrativeLevel2:string;
       administrativeLevel1:string;

}