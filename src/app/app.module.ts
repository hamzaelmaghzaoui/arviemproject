import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import {HttpCallService} from '../app/services/http-call.service'
import { JQUERY_PROVIDER } from './services/j-query.service';
import {DataTableModule} from 'angular-6-datatable';
import {MaterialModule} from './material/material.module';
import { LocationListComponent } from './location-list/location-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalComponent } from './modal/modal.component';
import { MatDialogModule } from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
    LocationListComponent,
    ModalComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    DataTableModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatDialogModule  

    
  ],
  providers: [HttpCallService,JQUERY_PROVIDER],
  bootstrap: [AppComponent],
  entryComponents: [ModalComponent]

})
export class AppModule {


 }
