import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Location } from '../model/location';
@Component({
  selector: 'arviem-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  //just an example for the object in to help testing the modal 
  location: Location = {
      "name": "Rotterdam",
      "normalizedName": "rotterdam",
      "coordinates": {
          "latitudeInDegrees": 51.950087,
          "longitudeInDegrees": 51.950087
      },
      "address": {
          "city": "Rotterdam",
          "country": "NL",
          "continent": "EUROPE",
          "streetName": null,
          "streetNumber": null,
          "postalCode": null,
          "administrativeLevel2": null,
          "administrativeLevel1": null
      },
      "resourceId": null,
      "version": null,
      "function": "SEAPORT"
  };

  constructor(
    public dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Location) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  
    
  ngOnInit() {
  }

}
