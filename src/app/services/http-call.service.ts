import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpCallService {

  prefixUrl: string = 'http://arviem-api.us-east-1.elasticbeanstalk.com';

  constructor(private http: HttpClient) { }

  callGetHttpRequest(URI: string,pagenumber: Number,pagesize: Number){
    return this.http.get(this.prefixUrl + URI + '?page='+pagenumber+'&pageSize='+pagesize);
  }

}
