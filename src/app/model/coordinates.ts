export class Coordinates { 
    latitudeInDegrees: Number; 
    longitudeInDegrees: Number;
}
